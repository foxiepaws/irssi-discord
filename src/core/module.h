/*
 * Filename:    module.h
 *
 * Description:
 *
 *
 * Version:     
 * Created:     Thu Feb 23 03:49:37 2017
 * Revision:    None
 * Author:      Allison Rachel Fox (foxiepaws), allisonthefox@gmail.com
 *
 */

#define MODULE_NAME "discord/core"

#include "irssi-config.h"
#include "common.h"
#include "discord.h"

